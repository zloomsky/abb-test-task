﻿using DataAccess.Contracts;
using Database;
using Database.Entities;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class MotorService : IMotorService
    {
        private readonly IDataContext _dataContext;

        public MotorService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IReadOnlyCollection<MotorDto> GetMotors()
        {
            return _dataContext
                .Get<Motor>()
                .Select(m => new MotorDto
                {
                    Id = m.Id,
                    Name = m.Name,
                    Type = (int)m.Type,
                    MaxPower = m.MaxPower,
                    Voltage = m.Voltage,
                    Current = m.Current,
                    FuelConsumption = m.FuelConsumption,
                    MaxTorque = m.MaxTorque,
                    MaxPressure = m.MaxPressure,
                    Displacement = m.Displacement
                })
                .ToArray();
        }

        public IReadOnlyCollection<MeasurementDto> GetMeasurements(int motorId)
        {
            return _dataContext
                .Get<Measurement>()
                .Where(x => x.MotorId == motorId)
                .Select(m => new MeasurementDto
                {
                    Id = m.Id,
                    Timestamp = m.Timestamp,
                    MotorName = m.Motor.Name,
                    Current = m.Current,
                    CurrentDifference = m.Motor.Current - m.Current,
                    MaxTorque = m.MaxTorque,
                    MaxTorqueDifference = m.Motor.MaxTorque - m.MaxTorque,
                    Pressure = m.Pressure,
                    PressureDifference = m.Motor.MaxPressure - m.Pressure
                })
                .ToArray();
        }

        public void UpdateMotor(MotorDto motorDto)
        {
            var motor = _dataContext.Get<Motor>().FirstOrDefault(x => x.Id == motorDto.Id);
            if (motor == null)
            {
                throw new System.Exception("Motor not found.");
            }

            motor.Name = motorDto.Name;
            motor.Type = (MotorType)motorDto.Type;
            motor.MaxPower = motorDto.MaxPower;
            motor.MaxPressure = motorDto.MaxPressure;
            motor.MaxTorque = motorDto.MaxTorque;
            motor.Voltage = motorDto.Voltage;
            _dataContext.Commit();
        }

        public void DeleteMotor(int motorId)
        {
            var motor = _dataContext.Get<Motor>().FirstOrDefault(x => x.Id == motorId);
            if (motor == null)
            {
                throw new System.Exception("Motor not found.");
            }
            _dataContext.Remove(motor);
            _dataContext.Commit();
        }
    }
}
