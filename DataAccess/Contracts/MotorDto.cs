﻿using System.Collections.Generic;
using System.Text;

namespace DataAccess.Contracts
{
    public class MotorDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public float MaxPower { get; set; }
        public float Voltage { get; set; }
        public float Current { get; set; }
        public float FuelConsumption { get; set; }
        public float MaxTorque { get; set; }
        public float MaxPressure { get; set; }
        public float Displacement { get; set; }
    }
}
