﻿using Database.Entities;
using System;

namespace DataAccess.Contracts
{
    public class MeasurementDto
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string MotorName { get; set; }
        public float Current { get; set; }
        public float CurrentDifference { get; set; }
        public float MaxTorque { get; set; }
        public float MaxTorqueDifference { get; set; }
        public float Pressure { get; set; }
        public float PressureDifference { get; set; }        
    }
}
