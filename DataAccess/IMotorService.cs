﻿using System.Collections.Generic;
using DataAccess.Contracts;

namespace DataAccess
{
    public interface IMotorService
    {
        void DeleteMotor(int motorId);
        IReadOnlyCollection<MeasurementDto> GetMeasurements(int motorId);
        IReadOnlyCollection<MotorDto> GetMotors();
        void UpdateMotor(MotorDto motorDto);
    }
}