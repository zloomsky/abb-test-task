﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database
{
    public class DataContextWrapper : IDataContext
    {
        private readonly DataContext _context;

        public DataContextWrapper(DataContext context)
        {
            _context = context;
        }

        public void Add<T>(T item) where T : class
        {
            _context.Set<T>().Add(item);
        }

        public void AddRange<T>(IEnumerable<T> items) where T : class
        {
            _context.Set<T>().AddRange(items);
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<T> Get<T>() where T : class
        {
            return _context.Set<T>();
        }

        public IReadOnlyCollection<T> Get<T>(string sql, params object[] parameters)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyCollection<T>> GetAsync<T>(string sql, params object[] parameters)
        {
            throw new System.NotImplementedException();
        }

        public void Remove<T>(T item) where T : class
        {
            _context.Set<T>().Remove(item);
        }

        public void RemoveRange<T>(IEnumerable<T> items) where T : class
        {
            _context.Set<T>().RemoveRange(items);
        }
    }
}
