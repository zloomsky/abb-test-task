﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database
{
    public interface IDataContext : IDisposable
    {
        IQueryable<T> Get<T>() where T : class;
        IReadOnlyCollection<T> Get<T>(string sql, params object[] parameters);
        Task<IReadOnlyCollection<T>> GetAsync<T>(string sql, params object[] parameters);
        void Add<T>(T item) where T : class;
        void AddRange<T>(IEnumerable<T> items) where T : class;
        void Remove<T>(T item) where T : class;
        void RemoveRange<T>(IEnumerable<T> items) where T : class;
        void Commit();
    }
}
