﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Database
{
    public class DataContext : DbContext
    {
        public DbSet<Motor> Motors { get; set; }

        public DbSet<Measurement> Measurements { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Motor>().HasMany(x => x.Measurements).WithOne(x => x.Motor).HasForeignKey(x => x.MotorId);          
            modelBuilder.Entity<Motor>().HasData(new[] {
                new Motor { Id = 1, Name = "Motor 1", Type = MotorType.Electric, MaxPower = 2, Voltage = 230, Current = 8.7F },
                new Motor { Id = 2, Name = "Motor 2", Type = MotorType.Combustion, MaxPower = 50, Voltage = 230, FuelConsumption = 4, MaxTorque = 3000 },
                new Motor { Id = 3, Name = "Motor 3", Type = MotorType.Hydraulic, MaxPower = 1, MaxPressure = 160, Displacement = 16 }
            });
            modelBuilder.Entity<Measurement>().HasData(new[] {           
                new Measurement { Id = 1, MotorId = 1, Timestamp = new DateTime(2019, 9, 1, 10, 0, 0, 0), Current = 7 },
                new Measurement { Id = 2, MotorId = 1, Timestamp = new DateTime(2019, 9, 1, 10, 10, 0, 0), Current = 7.9F },
                new Measurement { Id = 3, MotorId = 1, Timestamp = new DateTime(2019, 9, 1, 10, 20, 0, 0), Current = 6.5F },
                new Measurement { Id = 4, MotorId = 1, Timestamp = new DateTime(2019, 9, 1, 10, 30, 0, 0), Current = 7.3F },
                new Measurement { Id = 5, MotorId = 1, Timestamp = new DateTime(2019, 9, 1, 10, 40, 0, 0), Current = 7.8F },
                new Measurement { Id = 6, MotorId = 1, Timestamp = new DateTime(2019, 9, 1, 10, 50, 0, 0), Current = 6.9F },
                new Measurement { Id = 7, MotorId = 1, Timestamp = new DateTime(2019, 9, 1, 11, 0, 0, 0), Current = 7 },
                new Measurement { Id = 8, MotorId = 2, Timestamp = new DateTime(2019, 9, 1, 10, 0, 0, 0), MaxTorque = 2890 },
                new Measurement { Id = 9, MotorId = 2, Timestamp = new DateTime(2019, 9, 1, 10, 10, 0, 0), MaxTorque = 3100 },
                new Measurement { Id = 10, MotorId = 2, Timestamp = new DateTime(2019, 9, 1, 10, 20, 0, 0), MaxTorque = 2800 },
                new Measurement { Id = 11, MotorId = 2, Timestamp = new DateTime(2019, 9, 1, 10, 30, 0, 0), MaxTorque = 2860 },
                new Measurement { Id = 12, MotorId = 2, Timestamp = new DateTime(2019, 9, 1, 10, 40, 0, 0), MaxTorque = 2875 },
                new Measurement { Id = 13, MotorId = 2, Timestamp = new DateTime(2019, 9, 1, 10, 50, 0, 0), MaxTorque = 2790 },
                new Measurement { Id = 14, MotorId = 2, Timestamp = new DateTime(2019, 9, 1, 11, 0, 0, 0), MaxTorque = 2900 },
                new Measurement { Id = 15, MotorId = 3, Timestamp = new DateTime(2019, 9, 1, 10, 0, 0, 0), Pressure = 155 },
                new Measurement { Id = 16, MotorId = 3, Timestamp = new DateTime(2019, 9, 1, 10, 10, 0, 0), Pressure = 158 },
                new Measurement { Id = 17, MotorId = 3, Timestamp = new DateTime(2019, 9, 1, 10, 20, 0, 0), Pressure = 140 },
                new Measurement { Id = 18, MotorId = 3, Timestamp = new DateTime(2019, 9, 1, 10, 30, 0, 0), Pressure = 145 },
                new Measurement { Id = 19, MotorId = 3, Timestamp = new DateTime(2019, 9, 1, 10, 40, 0, 0), Pressure = 159 },
                new Measurement { Id = 20, MotorId = 3, Timestamp = new DateTime(2019, 9, 1, 10, 50, 0, 0), Pressure = 160 },
                new Measurement { Id = 21, MotorId = 3, Timestamp = new DateTime(2019, 9, 1, 11, 0, 0, 0), Pressure = 139 }
            });
        }
    }
}
