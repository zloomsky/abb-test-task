﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public class Measurement
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string MotorName { get; set; }
        public float Current { get; set; }
        public float MaxTorque { get; set; }
        public float Pressure { get; set; }
        public int MotorId { get; set; }
        public virtual Motor Motor { get; set; }
    }
}
