﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public class Motor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public MotorType Type { get; set; }
        public float MaxPower { get; set; }
        public float Voltage { get; set; }
        public float Current { get; set; }
        public float FuelConsumption { get; set; }
        public float MaxTorque { get; set; }
        public float MaxPressure { get; set; }
        public float Displacement { get; set; }
        public virtual ICollection<Measurement> Measurements { get; set; }
    }
}
