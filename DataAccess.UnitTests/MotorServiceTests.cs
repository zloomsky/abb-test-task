using Database;
using Database.Entities;
using Moq;
using System;
using Xunit;
using System.Linq;

namespace DataAccess.UnitTests
{
    public class MotorServiceTests
    {
        private readonly Mock<IDataContext> _dataContextMock;
        private readonly MotorService _sut;

        public MotorServiceTests()
        {
            _dataContextMock = new Mock<IDataContext>();
            _sut = new MotorService(_dataContextMock.Object);          
        }


        [Fact]
        public void GetMeasurements_MeasurementExist_ExpectedPressureDifference()
        {
            //Arrange
            var measurement = new Measurement { Pressure = 10, Motor = new Motor { MaxPressure = 13 } };
            _dataContextMock.Setup(d => d.Get<Measurement>()).Returns(new[] { measurement }.AsQueryable());

            //Act
            var measurements = _sut.GetMeasurements(0);

            //Assert
            Assert.Single(measurements);
            Assert.Equal(3, measurements.First().PressureDifference);
        }
    }
}
