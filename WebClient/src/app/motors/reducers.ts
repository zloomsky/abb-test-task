import { createReducer } from "typesafe-actions";
import { objectMap, tap } from "../../utils/object";
import { deleteMotor, getMeasurements, getMotors, updateMotor } from "./actions";
import { DefaultState } from "./models";

const reducer = createReducer(DefaultState)
  .handleAction(getMotors.success, (s, a) => ({ ...s, motors: objectMap(a.payload, t => t.id) }))
  .handleAction(getMeasurements.success, (s, a) => ({ ...s, measurements: objectMap(a.payload, t => t.id) }))
  .handleAction(updateMotor.success, (s, a) => ({ ...s, motors: { ...s.motors, [a.payload.id]: a.payload } }))
  .handleAction(deleteMotor.success, (s, a) => ({ ...s, motors: { ...tap(s.motors, m => delete m[a.payload]) }}))
  .handleAction(getMeasurements.request, (s, _) => ({ ...s, measurements: {} }));

export default reducer;
