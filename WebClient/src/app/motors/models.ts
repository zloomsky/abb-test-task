export interface Motor {
  id: number;
  name: string;
  type: string;
  maxPower: string;
  voltage: string;
  current: string;
  fuleConsumption: string;
  maxTorque: string;
  maxPressure: string;
  displacement: string;
}

export interface Measurement {
  id: number;
  timestamp: Date;
  motorName: string;
  current: number;
  currentDifference: number;
  maxTorque: number;
  maxTorqueDifference: number;
  pressure: number;
  pressureDifference: number;
}

export interface MotorsState {
  motors: { [key: number]: Motor };
  measurements: { [key: number]: Measurement };
}

export const DefaultState: MotorsState = {
  motors: {},
  measurements: {},
};
