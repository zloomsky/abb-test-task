import { asyncAction } from "../../utils/actions";
import { Measurement, Motor } from "./models";

const getMotors = asyncAction<undefined, Motor[]>("GET_MOTORS");
const updateMotor = asyncAction<Motor, Motor>("UPDATE_MOTOR");
const deleteMotor = asyncAction<number, number>("DELETE_MOTOR");
const getMeasurements = asyncAction<number, Measurement[]>("GET_MEASUREMENTS");

export { getMotors, updateMotor, deleteMotor, getMeasurements };
