import Timer from "@material-ui/icons/Timer";
import MaterialTable, { Column } from "material-table";
import React, { useEffect } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { ui } from "../../../shared";
import { Connector, Push } from "../../../utils/actions";
import { deleteMotor, getMotors, updateMotor } from "../actions";
import { Motor } from "../models";
import { tableIcons } from "./icons";

const connector = new Connector(a => ({ motors: a.motors.motors }), {
  getMotors: getMotors.request,
  updateMotor: updateMotor.request,
  deleteMotor: deleteMotor.request,
});

type Props = typeof connector.allProps & RouteComponentProps<{}>;

const columns: Column<Motor>[] = [
  { title: "Motor name", field: "name" },
  { title: "Type", field: "type", lookup: { 0: "Electric", 1: "Combustion", 2: "Hydraulic" } },
  { title: "Max power (kW)", field: "maxPower", type: "numeric" },
  { title: "Voltage (V)", field: "voltage" },
  { title: "Current (A)", field: "current" },
  { title: "Fuel consumtion per hour(l/h)", field: "fuleConsumption" },
  { title: "Max torque at (rpm)", field: "maxTorque" },
  { title: "Max presure (bar)", field: "maxPressure" },
  { title: "Displacement (cm3/rev)", field: "displacement" },
];

const detailsAction = (p: Push) => (row: Motor) => {
  return {
    icon: () => <Timer />,
    tooltip: "Measurements",
    onClick: () => p(ui.toMeasurements(row.id)),
  };
};

const Motors = (props: Props) => {
  const { } = props;

  useEffect(() => {
    props.getMotors();
  }, []);

  return (
    <MaterialTable
      style={{ width: "70vw" }}
      title="Motors"
      icons={tableIcons as any}
      columns={columns}
      data={Object.values(props.motors)}
      editable={{
        onRowUpdate: async (row: Motor) => { props.updateMotor(row); },
        onRowDelete: async (row: Motor) => { props.deleteMotor(row.id); },
      }}
      actions={[detailsAction(props.history.push)]}
    />
  );
};

export default withRouter(connector.connect()(Motors));
