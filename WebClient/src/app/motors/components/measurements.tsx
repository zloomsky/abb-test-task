import { Button } from "@material-ui/core";
import MaterialTable, { Column } from "material-table";
import moment from "moment";
import React, { useEffect } from "react";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { Connector, Push } from "../../../utils/actions";
import { getMeasurements } from "../actions";
import { Measurement } from "../models";
import { tableIcons } from "./icons";

const connector = new Connector(a => ({ measurements: a.motors.measurements }), {
  getMeasurements: getMeasurements.request,
});

type Props = typeof connector.allProps & RouteComponentProps<{ motorId?: string; }>;

const columns: Column<Measurement>[] = [
  { title: "TimeStamp ", render: r => moment(r.timestamp).format("h:mm:ss") },
  { title: "Motor ", field: "motorName" },
  { title: "Current (A)", field: "current" },
  { title: "Difference", field: "currentDifference" },
  { title: "Max torque at (rpm)", field: "maxTorque" },
  { title: "Difference", field: "maxTorqueDifference" },
  { title: "Max presure (bar)", field: "maxTorque" },
  { title: "Difference", field: "maxTorqueDifference" },
];

const backAction = (push: Push) => () => (
  <Button
    style={{ marginLeft: 15 }}
    onClick={() => push("")}
    color="primary"
    variant="contained">
    Back
  </Button>
);

const Measurements = (props: Props) => {

  useEffect(() => {
    props.getMeasurements(Number(props.match.params.motorId));
  }, []);

  return (
    <MaterialTable
      style={{ width: "70vw" }}
      title="Measurements"
      icons={tableIcons as any}
      columns={columns}
      actions={[{ isFreeAction: true, icon: "", onClick: () => void (0) }]}
      data={Object.values(props.measurements)}
      components={{ Action: backAction(props.history.push) }}
    />
  );

};

export default withRouter(connector.connect()(Measurements));
