import { api, http } from "../../shared";
import { requestEpic } from "../../utils/actions";
import { tapAsync } from "../../utils/object";
import { deleteMotor, getMeasurements, getMotors, updateMotor } from "./actions";

const getMotorsEpic = requestEpic(getMotors, () => http.getJsonAsync(api.getMotors));
const getMeasurementsEpic = requestEpic(getMeasurements, id => http.getJsonAsync(api.getMeasurements(id)));
const updateMotorEpic = requestEpic(updateMotor, tapAsync(m => http.postJsonAsync(api.updateMotor, m)));
const deteleMotorEpic = requestEpic(deleteMotor, tapAsync(m => http.deleteAsync(api.deleteMotor(m))));

export { getMotorsEpic, getMeasurementsEpic, updateMotorEpic, deteleMotorEpic };
