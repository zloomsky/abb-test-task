import { createMuiTheme, MuiThemeProvider } from "@material-ui/core";
import React, { CSSProperties } from "react";

const theme = createMuiTheme({
  palette: {
    type: "light",
  },
});

const root: CSSProperties = {
  width: "100vw",
  height: "100vh",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
};

const Layout = (props: any) => {
  return (
    <MuiThemeProvider theme={theme}>
      <div style={root}>
        {props.children}
      </div>
    </MuiThemeProvider>
  );
};

export default Layout;
