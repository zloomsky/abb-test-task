import { createReducer } from "typesafe-actions";
import { DefaultState } from "./models";

const reducer = createReducer(DefaultState);

export default reducer;
