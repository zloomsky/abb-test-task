import CssBaseline from "@material-ui/core/CssBaseline";
import React from "react";
import { Route, Switch } from "react-router-dom";
import Layout from "./app/layout/components";
import Measurements from "./app/motors/components/measurements";
import Motors from "./app/motors/components/motors";

const Routes = () => {
  return (
    <>
      <CssBaseline />
      <Layout>
        <Switch>
          <Route exact path="/" component={Motors} />
          <Route exact path="/:motorId" component={Measurements} />
        </Switch>
      </Layout>
    </>
  );
};

export default Routes;
