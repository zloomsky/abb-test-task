import { Middleware } from "redux";

export const logger: Middleware = ({ dispatch, getState }) => {
  return next => action => {
    const state = next(action);
    console.log(action.type, action.payload, getState());
    return state;
  };
};
