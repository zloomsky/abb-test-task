export const objectMap = <T, K>(src: T[], selector: (t: T) => K): { [key: string]: T } => {
  return src.reduce((acc, item) => ({ ...acc, [selector(item).toString()]: item }), {});
};

export const tap = <T>(object: T, action: (o: T) => void) => {
  action(object);
  return object;
};

export const tapAsync = <T>(action: (o: T) => Promise<any>) => async (object: T) => {
  await action(object);
  return object;
};
