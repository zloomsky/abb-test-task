const toMeasurements = (i: number) => `/${i}`;

const api = {
  toMeasurements,
};

export default api;
