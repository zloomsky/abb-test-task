const a = "api";
const getMotors = `${a}/motors`;
const getMeasurements = (i: number) => `${getMotors}/${i}`;
const deleteMotor = (i: number) => `${getMotors}/${i}`;
const updateMotor = `${getMotors}`;

const api = {
  getMotors,
  getMeasurements,
  updateMotor,
  deleteMotor,
};

export default api;
