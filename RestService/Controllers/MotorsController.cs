﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using DataAccess.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace RestService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MotorsController : ControllerBase
    {
        private readonly IMotorService _motorsService;

        public MotorsController(IMotorService motorsService)
        {
            _motorsService = motorsService;
        }

        [HttpGet]
        public ActionResult<IReadOnlyCollection<MotorDto>> GetMotors()
        {
            return Ok(_motorsService.GetMotors());
        }

        [HttpGet("{motorId}")]
        public ActionResult<IReadOnlyCollection<MeasurementDto>> GetMeasurements(int motorId)
        {
            return Ok(_motorsService.GetMeasurements(motorId));
        }

        [HttpPost]
        public void UpdateMotor([FromBody] MotorDto value)
        {
            _motorsService.UpdateMotor(value);
        }

        [HttpDelete("{motorId}")]
        public void DeleteMotor(int motorId)
        {
            _motorsService.DeleteMotor(motorId);
        }
    }
}
