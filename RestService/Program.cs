﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace RestService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Mvc", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.ColoredConsole(outputTemplate: "{Level:u3} {SourceContext:l} {Message:lj}{NewLine}{Exception}")
                .WriteTo.Debug(outputTemplate: "{Level:u3} {SourceContext:l} {Message:lj}{NewLine}{Exception}")
                .CreateLogger();

            Trace.Listeners.Add(new SerilogTraceListener.SerilogTraceListener("Trace"));

            try
            {
                var builder = WebHost.CreateDefaultBuilder(args).SuppressStatusMessages(true).UseStartup<Startup>();
                Trace.TraceInformation($"Starting web host on {builder.GetSetting(WebHostDefaults.ServerUrlsKey)}");

                builder.Build().Run();
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Host terminated unexpectedly {ex}");
            }
        }       
    }
}
